/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package qltv;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import org.apache.poi.xwpf.usermodel.ParagraphAlignment;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableCell;
import org.apache.poi.xwpf.usermodel.XWPFTableCell.XWPFVertAlign;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTJc;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTblPr;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STJc;
import qltv.entity.ChiTietMuonTra;
import qltv.entity.DocGia;
import qltv.entity.MuonTra;
import qltv.entity.NhanVien;
import qltv.entity.Sach;
import qltv.entity.TK;
import qltv.entity.TKR;

/**
 *
 * @author Kira
 */
public class WordHelper {

    public static void writeTK(File file, ArrayList<TK> list, String tittle, String thuocTinh) throws FileNotFoundException, IOException {
        FileOutputStream out;
        try (XWPFDocument document = loadHeader(tittle)) {
            createTableTK(document, list, thuocTinh);
            loadFooter(document);
            out = new FileOutputStream(file);
            document.write(out);//ghi lại
        }
        out.close();
    }

    public static void writeTKR(File file, ArrayList<TKR> list, String tittle, String thuocTinh) throws FileNotFoundException, IOException {
        FileOutputStream out;
        try (XWPFDocument document = loadHeader(tittle)) {
            createTableTKR(document, list, thuocTinh);
            loadFooter(document);
            out = new FileOutputStream(file);
            document.write(out);//ghi lại
        }
        out.close();
    }

    public static void writeDG(File file, ArrayList<DocGia> list, String tittle) throws FileNotFoundException, IOException {
        FileOutputStream out;
        try (XWPFDocument document = loadHeader(tittle)) {
            createTableDG(document, list);
            loadFooter(document);
            out = new FileOutputStream(file);
            document.write(out);//ghi lại
        }
        out.close();
    }

    public static void writeNV(File file, ArrayList<NhanVien> list, String tittle) throws FileNotFoundException, IOException {
        FileOutputStream out;
        try (XWPFDocument document = loadHeader(tittle)) {
            createTableNV(document, list);
            loadFooter(document);
            out = new FileOutputStream(file);
            document.write(out);//ghi lại
        }
        out.close();
    }

    public static void writeSach(File file, ArrayList<Sach> list, String tittle) throws FileNotFoundException, IOException {
        FileOutputStream out;
        try (XWPFDocument document = loadHeader(tittle)) {
            createTableSach(document, list);
            loadFooter(document);
            out = new FileOutputStream(file);
            document.write(out);//ghi lại
        }
        out.close();
    }

    public static void writeMT(File file, ArrayList<MuonTra> list, String tittle) throws FileNotFoundException, IOException {
        FileOutputStream out;
        try (XWPFDocument document = loadHeader(tittle)) {
            createTableMT(document, list);
            loadFooter(document);
            out = new FileOutputStream(file);
            document.write(out);//ghi lại
        }
        out.close();
    }

    public static void writeCTMT(File file, ArrayList<ChiTietMuonTra> list,
            String tittle, String maMT, boolean isAll) throws FileNotFoundException, IOException {
        FileOutputStream out;
        try (XWPFDocument document = loadHeader(tittle)) {
            MuonTra muonTra = MuonTra.get(maMT);
            XWPFParagraph paragraph = document.createParagraph();
            loadContentMuonTraChiTiet(paragraph, "Mã mượn trả : " + muonTra.getMa());
            loadContentMuonTraChiTiet(paragraph, "Mã độc giả : " + muonTra.getMaDocGia()
                    + "    -    Tên độc giả : " + DocGia.get(muonTra.getMaDocGia()).getTen());
            loadContentMuonTraChiTiet(paragraph, "Mã nhân viên : " + muonTra.getMaNhanVien()
                    + "    -    Tên nhân viên : " + NhanVien.get(muonTra.getMaNhanVien()).getTen());
            loadContentMuonTraChiTiet(paragraph, "Ngày mượn : "
                    + new SimpleDateFormat("dd-MM-yyyy").format(muonTra.getNgayMuon()));
            loadContentMuonTraChiTiet(paragraph, "Ngày hẹn trả : "
                    + new SimpleDateFormat("dd-MM-yyyy").format(muonTra.getNgayHenTra()));
            loadContentMuonTraChiTiet(paragraph, "Tiền cọc : " + muonTra.getTienCoc());
            //vẽ bảng
            long tongTienPhat = createTableCTMT(document, list);
            // tỉnh tiền phạt, tiền phải nộp
            if (isAll) {
                XWPFParagraph paragraphTinhTien = document.createParagraph();
                createRunAlignRight(paragraphTinhTien, "Tổng tiền phạt : " + tongTienPhat);
                long conPhaiTra = tongTienPhat - muonTra.getTienCoc();
                if (conPhaiTra > 0) {
                    createRunAlignRight(paragraphTinhTien, "Còn phải trả : " + conPhaiTra);
                } else {
                    createRunAlignRight(paragraphTinhTien, "Còn phải trả : 0");
                }
            }
            // gán phần đuôi
            loadFooter(document);
            out = new FileOutputStream(file);
            document.write(out);//ghi lại
        }
        out.close();
    }

    /**
     * @param b : là thể hiện có in đậm hay không tham số s là nội dung
     * @param s : set nội dung
     */
    private static void format(XWPFTableCell cell, String s, boolean b) {
        cell.setVerticalAlignment(XWPFVertAlign.CENTER);// chính giữa theo chiều cao
        XWPFParagraph p = cell.getParagraphs().get(0);//lấy doạn văn bản
        p.setIndentationLeft(200);// tương đượng padding left
        p.setIndentationRight(200);// tương đượng padding right
        p.setAlignment(ParagraphAlignment.CENTER);// căn giữa văn bản
        XWPFRun r = p.createRun();// nội dung
        r.setBold(b);
        r.setFontFamily("Times New Roman");//set Kiểu chữ
        r.setFontSize(13);//set size text
        r.setColor("000000");// set color text
        r.setText(s);// set content text
    }

    private static XWPFDocument loadHeader(String tittle) throws FileNotFoundException, IOException {
        FileInputStream fis = new FileInputStream(new File("C:\\Users\\Kira\\Documents\\tittle.docx"));
        // load header
        Calendar calendar = Calendar.getInstance();
        XWPFDocument document = new XWPFDocument(fis);
        
        List<XWPFParagraph> paragraphList = document.getParagraphs();
        
        XWPFParagraph paragraphTime = paragraphList.get(3);
        
        XWPFRun runTime = paragraphTime.createRun();
        runTime.setItalic(true);
        runTime.setFontFamily("Times New Roman");//set Kiểu chữ
        runTime.setFontSize(12);//set size text
        runTime.setColor("000000");// set color text
        runTime.setText("                             "
                + "                              "
                + "                                   "
                + "                     Ngày " + calendar.get(Calendar.DATE)
                + " tháng " + (calendar.get(Calendar.MONTH) + 1)
                + " năm " + calendar.get(Calendar.YEAR));// set content text
        runTime.addBreak();
        runTime.addBreak();
        runTime.addBreak();

//        for (int i = 0; i < paragraphList.size(); i++) {
//            System.out.println(i + paragraphList.get(i).getText());
//        }
        // tạo tiêu đề biểu mẫu
        XWPFParagraph paragraphTittle = document.createParagraph();
        
        paragraphTittle.setAlignment(ParagraphAlignment.CENTER);
        
        XWPFRun runTittle = paragraphTittle.createRun();
        runTittle.setBold(true);
        runTittle.setItalic(true);
        runTittle.setFontFamily("Times New Roman");//set Kiểu chữ
        runTittle.setFontSize(16);//set size text
        runTittle.setColor("000000");// set color text
        runTittle.setText(tittle);// set content text
        runTittle.addBreak();
        return document;
    }

    private static void createTableDG(XWPFDocument document, ArrayList<DocGia> list) {
        // tạo bảng 
        XWPFTable table = document.createTable();
        setTableAlign(table, ParagraphAlignment.CENTER);
        // khi tạo 1 bảng mới thì bảng chỉ có 1 dòng và 1 cột -> row 0, col 0
        //get first row - viết tittle
        XWPFTableRow tittleRow = table.getRow(0);
        format(tittleRow.getCell(0), "TT", true);
        tittleRow.getCell(0).getCTTc().addNewTcPr().addNewTcW().setW(BigInteger.valueOf(1000)); // set chiều rộng

        createNewCell(tittleRow, "Mã", 1000, 1);
        createNewCell(tittleRow, "Họ và tên", 2500, 2);
        createNewCell(tittleRow, "Số điện thoại", 2500, 3);
        createNewCell(tittleRow, "Địa chỉ", 6500, 4);

        // đọc dữ liệu
        for (int i = 0; i < list.size(); i++) {
            DocGia o = list.get(i);
            XWPFTableRow row = table.createRow();// tạo dòng mới
            format(row.getCell(0), (i + 1) + "", false);
            format(row.getCell(1), o.getMa() + "", false);
            format(row.getCell(2), o.getTen() + "", false);
            format(row.getCell(3), "0" + o.getSdt(), false);
            format(row.getCell(4), o.getDiaChi() + "", false);
        }
    }

    private static void createTableNV(XWPFDocument document, ArrayList<NhanVien> list) {
        // tạo bảng 
        XWPFTable table = document.createTable();
        setTableAlign(table, ParagraphAlignment.CENTER);
        // khi tạo 1 bảng mới thì bảng chỉ có 1 dòng và 1 cột -> row 0, col 0
        //get first row - viết tittle
        XWPFTableRow tittleRow = table.getRow(0);
        format(tittleRow.getCell(0), "TT", true);
        tittleRow.getCell(0).getCTTc().addNewTcPr().addNewTcW().setW(BigInteger.valueOf(1000)); // set chiều rộng

        createNewCell(tittleRow, "Mã", 1000, 1);
        createNewCell(tittleRow, "Họ và tên", 2500, 2);
        createNewCell(tittleRow, "Số điện thoại", 2500, 3);
        createNewCell(tittleRow, "Địa chỉ", 6500, 4);

        // đọc dữ liệu
        for (int i = 0; i < list.size(); i++) {
            NhanVien o = list.get(i);
            XWPFTableRow row = table.createRow();// tạo dòng mới
            format(row.getCell(0), (i + 1) + "", false);
            format(row.getCell(1), o.getMa() + "", false);
            format(row.getCell(2), o.getTen() + "", false);
            format(row.getCell(3), "0" + o.getSdt(), false);
            format(row.getCell(4), o.getDiaChi() + "", false);
        }
    }

    private static void createTableSach(XWPFDocument document, ArrayList<Sach> list) {
        // tạo bảng 
        XWPFTable table = document.createTable();
        setTableAlign(table, ParagraphAlignment.CENTER);
        // khi tạo 1 bảng mới thì bảng chỉ có 1 dòng và 1 cột -> row 0, col 0
        //get first row - viết tittle
        XWPFTableRow tittleRow = table.getRow(0);
        format(tittleRow.getCell(0), "TT", true);
        tittleRow.getCell(0).getCTTc().addNewTcPr().addNewTcW().setW(BigInteger.valueOf(300)); // set chiều rộng

        createNewCell(tittleRow, "Mã", 300, 1);
        createNewCell(tittleRow, "Tên sách", 2500, 2);
        createNewCell(tittleRow, "Loại sách", 2500, 3);
        createNewCell(tittleRow, "Tác giả", 2500, 4);
        createNewCell(tittleRow, "Nhà XB", 2500, 5);
        createNewCell(tittleRow, "Năm XB", 1000, 6);
        createNewCell(tittleRow, "Vị trí", 2500, 7);
        createNewCell(tittleRow, "Số lượng", 300, 8);

        // đọc dữ liệu
        for (int i = 0; i < list.size(); i++) {
            Sach o = list.get(i);
            XWPFTableRow row = table.createRow();// tạo dòng mới
            format(row.getCell(0), (i + 1) + "", false);
            format(row.getCell(1), o.getMa() + "", false);
            format(row.getCell(2), o.getTen() + "", false);
            format(row.getCell(3), o.getLoaiSach() + "", false);
            format(row.getCell(4), o.getTacGia() + "", false);
            format(row.getCell(5), o.getNhaXuatBan() + "", false);
            format(row.getCell(6), o.getNamXuatBan() + "", false);
            format(row.getCell(7), o.getViTri() + "", false);
            format(row.getCell(8), o.getSoLuong() + "", false);
        }
    }

    private static void createTableMT(XWPFDocument document, ArrayList<MuonTra> list) {
        // tạo bảng 
        XWPFTable table = document.createTable();
        setTableAlign(table, ParagraphAlignment.CENTER);
        // khi tạo 1 bảng mới thì bảng chỉ có 1 dòng và 1 cột -> row 0, col 0
        //get first row - viết tittle
        XWPFTableRow tittleRow = table.getRow(0);
        format(tittleRow.getCell(0), "TT", true);
        tittleRow.getCell(0).getCTTc().addNewTcPr().addNewTcW().setW(BigInteger.valueOf(500)); // set chiều rộng
//
        createNewCell(tittleRow, "Mã", 500, 1);
        createNewCell(tittleRow, "Độc giả", 2500, 2);
        createNewCell(tittleRow, "Nhân viên", 2500, 3);
        createNewCell(tittleRow, "Ngày mượn", 2500, 4);
        createNewCell(tittleRow, "Ngày hẹn trả", 2500, 5);
        createNewCell(tittleRow, "Tiền cọc", 2500, 6);

        // đọc dữ liệu
        for (int i = 0; i < list.size(); i++) {
            MuonTra o = list.get(i);
            XWPFTableRow row = table.createRow();// tạo dòng mới
            format(row.getCell(0), (i + 1) + "", false);
            format(row.getCell(1), o.getMa() + "", false);
            format(row.getCell(2), DocGia.get(o.getMaDocGia()).getTen() + "", false);
            format(row.getCell(3), NhanVien.get(o.getMaNhanVien()).getTen() + "", false);
            format(row.getCell(4), new SimpleDateFormat("dd-MM-yyyy")
                    .format(o.getNgayMuon()), false);
            format(row.getCell(5), new SimpleDateFormat("dd-MM-yyyy")
                    .format(o.getNgayHenTra()), false);
            format(row.getCell(6), o.getTienCoc() + "", false);
        }
    }

    private static long createTableCTMT(XWPFDocument document, ArrayList<ChiTietMuonTra> list) {
        long tongTienPhat = 0;
        // tạo bảng 
        XWPFTable table = document.createTable();
        setTableAlign(table, ParagraphAlignment.CENTER);
        // khi tạo 1 bảng mới thì bảng chỉ có 1 dòng và 1 cột -> row 0, col 0
        //get first row - viết tittle
        XWPFTableRow tittleRow = table.getRow(0);
        format(tittleRow.getCell(0), "TT", true);
        tittleRow.getCell(0).getCTTc().addNewTcPr().addNewTcW().setW(BigInteger.valueOf(500)); // set chiều rộng
//
        createNewCell(tittleRow, "Mã mượn trả", 1000, 1);
        createNewCell(tittleRow, "Sách", 3000, 2);
        createNewCell(tittleRow, "Số lượng", 500, 3);
        createNewCell(tittleRow, "Ngày trả", 2000, 4);
        createNewCell(tittleRow, "Tiền phạt", 1500, 5);

        // đọc dữ liệu
        for (int i = 0; i < list.size(); i++) {
            ChiTietMuonTra o = list.get(i);
            XWPFTableRow row = table.createRow();// tạo dòng mới
            format(row.getCell(0), (i + 1) + "", false);
            format(row.getCell(1), o.getMaMuonTra(), false);
            format(row.getCell(2), Sach.get(o.getMaSach()).getTen() + "", false);
            format(row.getCell(3), o.getSoLuong() + "", false);
            if (o.getNgayTra() != null) {
                format(row.getCell(4), new SimpleDateFormat("dd-MM-yyyy")
                        .format(o.getNgayTra()) + "", false);
                format(row.getCell(5), o.getTienPhat() + "", false);
                tongTienPhat += o.getTienPhat();
            } else {
                format(row.getCell(4), "", false);
                format(row.getCell(5), "", false);
            }
        }
        return tongTienPhat;
    }

    private static void createTableTK(XWPFDocument document, ArrayList<TK> list, String thuocTinh) {
        // tạo bảng 
        XWPFTable table = document.createTable();
        setTableAlign(table, ParagraphAlignment.CENTER);
        // khi tạo 1 bảng mới thì bảng chỉ có 1 dòng và 1 cột -> row 0, col 0
        //get first row - viết tittle
        XWPFTableRow tittleRow = table.getRow(0);
        format(tittleRow.getCell(0), "TT", true);
        tittleRow.getCell(0).getCTTc().addNewTcPr().addNewTcW().setW(BigInteger.valueOf(3000)); // set chiều rộng

        createNewCell(tittleRow, thuocTinh, 3000, 1);
        createNewCell(tittleRow, "Số lượng", 3000, 2);

        // đọc dữ liệu
        for (int i = 0; i < list.size(); i++) {
            TK o = list.get(i);
            XWPFTableRow row = table.createRow();// tạo dòng mới
            format(row.getCell(0), (i + 1) + "", false);
            format(row.getCell(1), o.getThuocTinh() + "", false);
            format(row.getCell(2), o.getSoLuong() + "", false);
        }
    }

    private static void createTableTKR(XWPFDocument document, ArrayList<TKR> list, String thuocTinh) {
        // tạo bảng 
        XWPFTable table = document.createTable();
        setTableAlign(table, ParagraphAlignment.CENTER);
        // khi tạo 1 bảng mới thì bảng chỉ có 1 dòng và 1 cột -> row 0, col 0
        //get first row - viết tittle
        XWPFTableRow tittleRow = table.getRow(0);
        format(tittleRow.getCell(0), "TT", true);
        tittleRow.getCell(0).getCTTc().addNewTcPr().addNewTcW().setW(BigInteger.valueOf(3000)); // set chiều rộng
        createNewCell(tittleRow, thuocTinh, 3000, 1);
        createNewCell(tittleRow, "Tổng tiền cọc", 3000, 2);
        createNewCell(tittleRow, "Tổng tiền phạt", 3000, 3);
        createNewCell(tittleRow, "Số lượng", 2000, 4);

        // đọc dữ liệu
        for (int i = 0; i < list.size(); i++) {
            TKR o = list.get(i);
            XWPFTableRow row = table.createRow();// tạo dòng mới
            format(row.getCell(0), (i + 1) + "", false);
            format(row.getCell(1), o.getTk().getThuocTinh() + "", false);
            format(row.getCell(2), o.getTienCoc() + "", false);
            format(row.getCell(3), o.getTienPhat() + "", false);
            format(row.getCell(4), o.getTk().getSoLuong() + "", false);
        }
    }

    private static void loadFooter(XWPFDocument document) {
        // tạo footer
        XWPFParagraph paragraphFooter = document.createParagraph();
//        paragraphFooter.setAlignment(ParagraphAlignment.CENTER);
        XWPFRun runF1 = paragraphFooter.createRun();
        runF1.addBreak();
        runF1.setBold(true);
        runF1.setFontFamily("Times New Roman");//set Kiểu chữ
        runF1.setFontSize(12);//set size text
        runF1.setColor("000000");// set color text
        runF1.setText("                           Người lập                                            "
                + "                               Xác nhận của thủ thư");// set content text
        runF1.addBreak();

        XWPFRun runF2 = paragraphFooter.createRun();
        runF2.setItalic(true);
        runF2.setFontFamily("Times New Roman");//set Kiểu chữ
        runF2.setFontSize(12);//set size text
        runF2.setColor("000000");// set color text
        runF2.setText("                   (Ký và ghi rõ họ tên)                                            "
                + "                         (Ký và ghi rõ họ tên)");// set content text
    }

    private static void createNewCell(XWPFTableRow tittleRow, String s, int i, int stt) {
        tittleRow.addNewTableCell();// tạo cell mới
        format(tittleRow.getCell(stt), s, true);
        tittleRow.getCell(stt).getCTTc().addNewTcPr().addNewTcW().setW(BigInteger.valueOf(i));
    }

    public static void setTableAlign(XWPFTable table, ParagraphAlignment align) {
        CTTblPr tblPr = table.getCTTbl().getTblPr();
        CTJc jc = (tblPr.isSetJc() ? tblPr.getJc() : tblPr.addNewJc());
        STJc.Enum en = STJc.Enum.forInt(align.getValue());
        jc.setVal(en);
    }

    public static void loadContentMuonTraChiTiet(XWPFParagraph paragraph, String s) {
        XWPFRun run = paragraph.createRun();
//        run.setBold(true);
        run.setFontFamily("Times New Roman");//set Kiểu chữ
        run.setFontSize(13);//set size text
        run.setColor("000000");// set color text
        run.setText("                   "
                + s);// set content text
        run.addBreak();
    }

    private static void createRunAlignRight(XWPFParagraph paragraphTinhTien, String content) {
        XWPFRun runF1 = paragraphTinhTien.createRun();
        runF1.addBreak();
        runF1.setBold(true);
        runF1.setFontFamily("Times New Roman");//set Kiểu chữ
        runF1.setFontSize(13);//set size text
        runF1.setColor("000000");// set color text
        runF1.setText("                          "
                + "                               "
                + "                           "
                + "                      " + content);// set content text
    }

}
