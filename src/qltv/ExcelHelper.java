/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package qltv;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import qltv.entity.ChiTietMuonTra;
import qltv.entity.DocGia;
import qltv.entity.MuonTra;
import qltv.entity.NhanVien;
import qltv.entity.Sach;

/**
 *
 * @author Kira
 */
public class ExcelHelper {

    public static ArrayList<DocGia> readDocGias(File file) throws Exception {
        ArrayList<DocGia> list = new ArrayList<>();
        // Đọc một file XSL.
        FileInputStream inputStream = new FileInputStream(file);
        String type = file.getName().substring(file.getName().lastIndexOf(".") + 1);
        Iterator<Row> rowIterator = null;
        if (type.equals("xls")) {
            // Đối tượng workbook cho file XSL.
            HSSFWorkbook workbook = new HSSFWorkbook(inputStream);
            // Lấy ra sheet đầu tiên từ workbook
            HSSFSheet sheet = workbook.getSheetAt(0);
            // Lấy ra Iterator cho tất cả các dòng của sheet hiện tại.
            rowIterator = sheet.iterator();
        } else if (type.equals("xlsx")) {
            // Đối tượng workbook cho file XLSX.
            XSSFWorkbook workbook = new XSSFWorkbook(inputStream);
            // Lấy ra sheet đầu tiên từ workbook
            XSSFSheet sheet = workbook.getSheetAt(0);
            // Lấy ra Iterator cho tất cả các dòng của sheet hiện tại.
            rowIterator = sheet.iterator();
        }
        rowIterator.next();// tránh dòng đầu - tên của các cột
        while (rowIterator.hasNext()) {
            Row row = rowIterator.next();
            // Lấy Iterator cho tất cả các cell của dòng hiện tại.
            Iterator<Cell> cellIterator = row.cellIterator();
            int i = 0;
            DocGia o = new DocGia();
            while (cellIterator.hasNext()) {
                Cell cell = cellIterator.next();
                switch (i) {
                    case 0:
                        o.setMa(cell.getStringCellValue());
                        i++;
                        break;
                    case 1:
                        o.setTen(cell.getStringCellValue());
                        i++;
                        break;
                    case 2:
                        o.setSdt((long) cell.getNumericCellValue());
                        i++;
                        break;
                    case 3:
                        o.setDiaChi(cell.getStringCellValue());
                        i++;
                        break;
                }
            }
            list.add(o);
        }
        return list;
    }

    public static ArrayList<NhanVien> readNhanViens(File file) throws Exception {
        ArrayList<NhanVien> list = new ArrayList<>();
        // Đọc một file XSL.
        FileInputStream inputStream = new FileInputStream(file);
        String type = file.getName().substring(file.getName().lastIndexOf(".") + 1);
        Iterator<Row> rowIterator = null;
        if (type.equals("xls")) {
            // Đối tượng workbook cho file XSL.
            HSSFWorkbook workbook = new HSSFWorkbook(inputStream);
            // Lấy ra sheet đầu tiên từ workbook
            HSSFSheet sheet = workbook.getSheetAt(0);
            // Lấy ra Iterator cho tất cả các dòng của sheet hiện tại.
            rowIterator = sheet.iterator();
        } else if (type.equals("xlsx")) {
            // Đối tượng workbook cho file XLSX.
            XSSFWorkbook workbook = new XSSFWorkbook(inputStream);
            // Lấy ra sheet đầu tiên từ workbook
            XSSFSheet sheet = workbook.getSheetAt(0);
            // Lấy ra Iterator cho tất cả các dòng của sheet hiện tại.
            rowIterator = sheet.iterator();
        }
        rowIterator.next();// tránh dòng đầu - tên của các cột
        while (rowIterator.hasNext()) {
            Row row = rowIterator.next();
            // Lấy Iterator cho tất cả các cell của dòng hiện tại.
            Iterator<Cell> cellIterator = row.cellIterator();
            int i = 0;
            NhanVien o = new NhanVien();
            while (cellIterator.hasNext()) {
                Cell cell = cellIterator.next();
                switch (i) {
                    case 0:
                        o.setMa(cell.getStringCellValue());
                        i++;
                        break;
                    case 1:
                        o.setTen(cell.getStringCellValue());
                        i++;
                        break;
                    case 2:
                        o.setSdt((long) cell.getNumericCellValue());
                        i++;
                        break;
                    case 3:
                        o.setDiaChi(cell.getStringCellValue());
                        i++;
                        break;
                }
            }
            list.add(o);
        }
        return list;
    }

    public static ArrayList<Sach> readSachs(File file) throws Exception {
        ArrayList<Sach> list = new ArrayList<>();
        FileInputStream inputStream = new FileInputStream(file);
        String type = file.getName().substring(file.getName().lastIndexOf(".") + 1);
        Iterator<Row> rowIterator = null;
        if (type.equals("xls")) {
            HSSFWorkbook workbook = new HSSFWorkbook(inputStream);
            HSSFSheet sheet = workbook.getSheetAt(0);
            rowIterator = sheet.iterator();
        } else if (type.equals("xlsx")) {
            XSSFWorkbook workbook = new XSSFWorkbook(inputStream);
            XSSFSheet sheet = workbook.getSheetAt(0);
            rowIterator = sheet.iterator();
        }
        rowIterator.next();// tránh dòng đầu - tên của các cột
        while (rowIterator.hasNext()) {
            Row row = rowIterator.next();
            // Lấy Iterator cho tất cả các cell của dòng hiện tại.
            Iterator<Cell> cellIterator = row.cellIterator();
            int i = 0;
            Sach o = new Sach();
            while (cellIterator.hasNext()) {
                Cell cell = cellIterator.next();
                switch (i) {
                    case 0:
                        o.setMa(cell.getStringCellValue());
                        i++;
                        break;
                    case 1:
                        o.setTen(cell.getStringCellValue());
                        i++;
                        break;
                    case 2:
                        o.setLoaiSach(cell.getStringCellValue());
                        i++;
                        break;
                    case 3:
                        o.setTacGia(cell.getStringCellValue());
                        i++;
                        break;
                    case 4:
                        o.setNhaXuatBan(cell.getStringCellValue());
                        i++;
                        break;
                    case 5:
                        o.setNamXuatBan((int) cell.getNumericCellValue());
                        i++;
                        break;
                    case 6:
                        o.setViTri(cell.getStringCellValue());
                        i++;
                        break;
                    case 7:
                        o.setSoLuong((int) cell.getNumericCellValue());
                        i++;
                        break;
                }
            }
            list.add(o);
        }
        return list;
    }

    public static ArrayList<MuonTra> readMuonTras(File file) throws Exception {
        ArrayList<MuonTra> list = new ArrayList<>();
        FileInputStream inputStream = new FileInputStream(file);
        String type = file.getName().substring(file.getName().lastIndexOf(".") + 1);
        Iterator<Row> rowIterator = null;
        if (type.equals("xls")) {
            HSSFWorkbook workbook = new HSSFWorkbook(inputStream);
            HSSFSheet sheet = workbook.getSheetAt(0);
            rowIterator = sheet.iterator();
        } else if (type.equals("xlsx")) {
            XSSFWorkbook workbook = new XSSFWorkbook(inputStream);
            XSSFSheet sheet = workbook.getSheetAt(0);
            rowIterator = sheet.iterator();
        }
        rowIterator.next();// tránh dòng đầu - tên của các cột
        while (rowIterator.hasNext()) {
            Row row = rowIterator.next();
            // Lấy Iterator cho tất cả các cell của dòng hiện tại.
            Iterator<Cell> cellIterator = row.cellIterator();
            int i = 0;
            MuonTra o = new MuonTra();
            while (cellIterator.hasNext()) {
                Cell cell = cellIterator.next();
                switch (i) {
                    case 0:
                        o.setMa(cell.getStringCellValue());
                        i++;
                        break;
                    case 1:
                        o.setMaDocGia(cell.getStringCellValue());
                        i++;
                        break;
                    case 2:
                        o.setMaNhanVien(cell.getStringCellValue());
                        i++;
                        break;
                    case 3:
                        o.setNgayMuon(cell.getDateCellValue());
                        i++;
                        break;
                    case 4:
                        o.setNgayHenTra(cell.getDateCellValue());
                        i++;
                        break;
                    case 5:
                        o.setTienCoc((int) cell.getNumericCellValue());
                        i++;
                        break;
                }
            }
            list.add(o);
        }
        return list;
    }

    public static ArrayList<ChiTietMuonTra> readChiTietMuonTras(File file) throws Exception {
        ArrayList<ChiTietMuonTra> list = new ArrayList<>();
        FileInputStream inputStream = new FileInputStream(file);
        String type = file.getName().substring(file.getName().lastIndexOf(".") + 1);
        Iterator<Row> rowIterator = null;
        if (type.equals("xls")) {
            HSSFWorkbook workbook = new HSSFWorkbook(inputStream);
            HSSFSheet sheet = workbook.getSheetAt(0);
            rowIterator = sheet.iterator();
        } else if (type.equals("xlsx")) {
            XSSFWorkbook workbook = new XSSFWorkbook(inputStream);
            XSSFSheet sheet = workbook.getSheetAt(0);
            rowIterator = sheet.iterator();
        }
        rowIterator.next();// tránh dòng đầu - tên của các cột
        while (rowIterator.hasNext()) {
            Row row = rowIterator.next();
            // Lấy Iterator cho tất cả các cell của dòng hiện tại.
            Iterator<Cell> cellIterator = row.cellIterator();
            int i = 0;
            ChiTietMuonTra o = new ChiTietMuonTra();
            while (cellIterator.hasNext()) {
                Cell cell = cellIterator.next();
                switch (i) {
                    case 0:
                        o.setMaMuonTra(cell.getStringCellValue());
                        i++;
                        break;
                    case 1:
                        o.setMaSach(cell.getStringCellValue());
                        i++;
                        break;
                    case 2:
                        o.setSoLuong((int) cell.getNumericCellValue());
                        i++;
                        break;
//                    case 3:
//                        o.setTienPhat((int) cell.getNumericCellValue());
//                        i++;
//                        break;
//                    case 4:
//                        o.setNgayTra(cell.getDateCellValue());
//                        i++;
//                        break;
                }
            }
            list.add(o);
        }
        return list;
    }
}
