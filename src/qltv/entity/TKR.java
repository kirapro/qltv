/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package qltv.entity;

/**
 *
 * @author Kira
 */
public class TKR {

    private TK tk;
    private long tienCoc;
    private long tienPhat;

    public TKR(TK tk, long tienCoc, long tienPhat) {
        this.tk = tk;
        this.tienCoc = tienCoc;
        this.tienPhat = tienPhat;
    }

    public TKR() {
    }

    public TK getTk() {
        return tk;
    }

    public void setTk(TK tk) {
        this.tk = tk;
    }

    public long getTienCoc() {
        return tienCoc;
    }

    public void setTienCoc(long tienCoc) {
        this.tienCoc = tienCoc;
    }

    public long getTienPhat() {
        return tienPhat;
    }

    public void setTienPhat(long tienPhat) {
        this.tienPhat = tienPhat;
    }

}
