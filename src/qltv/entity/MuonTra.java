/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package qltv.entity;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import qltv.ConnectDatabase;

/**
 *
 * @author Kira
 */
public class MuonTra {

    private String ma;
    private String maDocGia;
    private String maNhanVien;
    private Date ngayMuon;
    private Date ngayHenTra;
    private int tienCoc;

    public MuonTra() {
    }

    public MuonTra(String ma, String maDocGia, String maNhanVien, Date ngayMuon, Date ngayHenTra, int tienCoc) {
        this.ma = ma;
        this.maDocGia = maDocGia;
        this.maNhanVien = maNhanVien;
        this.ngayMuon = ngayMuon;
        this.ngayHenTra = ngayHenTra;
        this.tienCoc = tienCoc;
    }

    public String getMa() {
        return ma;
    }

    public void setMa(String ma) {
        this.ma = ma;
    }

    public String getMaDocGia() {
        return maDocGia;
    }

    public void setMaDocGia(String maDocGia) {
        this.maDocGia = maDocGia;
    }

    public String getMaNhanVien() {
        return maNhanVien;
    }

    public void setMaNhanVien(String maNhanVien) {
        this.maNhanVien = maNhanVien;
    }

    public Date getNgayMuon() {
        return ngayMuon;
    }

    public void setNgayMuon(Date ngayMuon) {
        this.ngayMuon = ngayMuon;
    }

    public Date getNgayHenTra() {
        return ngayHenTra;
    }

    public void setNgayHenTra(Date ngayHenTra) {
        this.ngayHenTra = ngayHenTra;
    }

    public int getTienCoc() {
        return tienCoc;
    }

    public void setTienCoc(int tienCoc) {
        this.tienCoc = tienCoc;
    }

    public static ArrayList<MuonTra> getList(String sql) {
        ArrayList<MuonTra> list = new ArrayList<>();
        ConnectDatabase connectDatabase = new ConnectDatabase();
        Connection connection = connectDatabase.getConnect();
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                list.add(new MuonTra(resultSet.getString("ma"),
                        resultSet.getString("madocgia"),
                        resultSet.getString("manhanvien"),
                        resultSet.getDate("ngaymuon"),
                        resultSet.getDate("ngayhentra"),
                        resultSet.getInt("tiencoc")));
            }
            connection.close();
        } catch (SQLException e) {
        }
        return list;
    }

    public static int add(MuonTra o) {
        String sql = "insert into muontra values('"
                + o.getMa() + "', '"
                + o.getMaDocGia() + "', '"
                + o.getMaNhanVien() + "', '"
                + new SimpleDateFormat("yyyy-MM-dd").format(o.getNgayMuon()) + "', '"
                + new SimpleDateFormat("yyyy-MM-dd").format(o.getNgayHenTra()) + "', "
                + o.getTienCoc()
                + ")";
        return interact(sql);
    }

    public static int update(MuonTra o) {
        String sql = "update muontra set madocgia = '"
                + o.getMaDocGia() + "', manhanvien = '"
                + o.getMaNhanVien() + "', ngaymuon = '"
                + new SimpleDateFormat("yyyy-MM-dd").format(o.getNgayMuon()) + "', ngayhentra = '"
                + new SimpleDateFormat("yyyy-MM-dd").format(o.getNgayHenTra()) + "', tiencoc = "
                + o.getTienCoc()
                + " where ma = '" + o.getMa() + "'";
        return interact(sql);
    }

    public static int delete(MuonTra o) {
        String sql = "delete from muontra where ma = '"
                + o.getMa() + "'";
        return interact(sql);
    }

    public static ArrayList<MuonTra> search(MuonTra o) {
        ArrayList<MuonTra> muonTras = new ArrayList<>();
        String sql = "select * from muontra where";
        if (!o.getMa().equals("")) {
            sql += " ma = '" + o.getMa() + "'";
            if (!o.getMaDocGia().equals("")) {
                sql += " and madocgia = '" + o.getMaDocGia() + "'";
            }
            if (!o.getMaNhanVien().equals("")) {
                sql += " and manhanvien = '" + o.getMaNhanVien() + "'";
            }
            if (!(new SimpleDateFormat("dd-MM-yyyy").format(o.getNgayMuon()).equals("01-01-1900"))) {
                sql += " and ngaymuon = '" + new SimpleDateFormat("yyyy-MM-dd")
                        .format(o.getNgayMuon()) + "'";
            }
            if (!(new SimpleDateFormat("dd-MM-yyyy").format(o.getNgayHenTra()).equals("01-01-1900"))) {
                sql += " and ngayhentra = '" + new SimpleDateFormat("yyyy-MM-dd")
                        .format(o.getNgayHenTra()) + "'";
            }
            if (o.getTienCoc() != -1) {
                sql += " and tiencoc = " + o.getTienCoc();
            }
        } else {
            if (!o.getMaDocGia().equals("")) {
                sql += " madocgia = '" + o.getMaDocGia() + "'";
                if (!o.getMaNhanVien().equals("")) {
                    sql += " and manhanvien = '" + o.getMaNhanVien() + "'";
                }
                if (!(new SimpleDateFormat("dd-MM-yyyy").format(o.getNgayMuon()).equals("01-01-1900"))) {
                    sql += " and ngaymuon = '" + new SimpleDateFormat("yyyy-MM-dd")
                            .format(o.getNgayMuon()) + "'";
                }
                if (!(new SimpleDateFormat("dd-MM-yyyy").format(o.getNgayHenTra()).equals("01-01-1900"))) {
                    sql += " and ngayhentra = '" + new SimpleDateFormat("yyyy-MM-dd")
                            .format(o.getNgayHenTra()) + "'";
                }
                if (o.getTienCoc() != -1) {
                    sql += " and tiencoc = " + o.getTienCoc();
                }
            } else {
                if (!o.getMaNhanVien().equals("")) {
                    sql += " manhanvien = '" + o.getMaNhanVien() + "'";
                    if (!(new SimpleDateFormat("dd-MM-yyyy").format(o.getNgayMuon()).equals("01-01-1900"))) {
                        sql += " and ngaymuon = '" + new SimpleDateFormat("yyyy-MM-dd")
                                .format(o.getNgayMuon()) + "'";
                    }
                    if (!(new SimpleDateFormat("dd-MM-yyyy").format(o.getNgayHenTra()).equals("01-01-1900"))) {
                        sql += " and ngayhentra = '" + new SimpleDateFormat("yyyy-MM-dd")
                                .format(o.getNgayHenTra()) + "'";
                    }
                    if (o.getTienCoc() != -1) {
                        sql += " and tiencoc = " + o.getTienCoc();
                    }
                } else {
                    if (!(new SimpleDateFormat("dd-MM-yyyy").format(o.getNgayMuon()).equals("01-01-1900"))) {
                        sql += " ngaymuon = '" + new SimpleDateFormat("yyyy-MM-dd")
                                .format(o.getNgayMuon()) + "'";
                        if (!(new SimpleDateFormat("dd-MM-yyyy").format(o.getNgayHenTra()).equals("01-01-1900"))) {
                            sql += " and ngayhentra = '" + new SimpleDateFormat("yyyy-MM-dd")
                                    .format(o.getNgayHenTra()) + "'";
                        }
                        if (o.getTienCoc() != -1) {
                            sql += " and tiencoc = " + o.getTienCoc();
                        }
                    } else {
                        if (!(new SimpleDateFormat("dd-MM-yyyy").format(o.getNgayHenTra()).equals("01-01-1900"))) {
                            sql += " ngayhentra = '" + new SimpleDateFormat("yyyy-MM-dd")
                                    .format(o.getNgayHenTra()) + "'";
                            if (o.getTienCoc() != -1) {
                                sql += " and tiencoc = " + o.getTienCoc();
                            }
                        } else {
                            if (o.getTienCoc() != -1) {
                                sql += " tiencoc = " + o.getTienCoc();
                            } else {
                                return muonTras;
                            }
                        }
                    }
                }
            }
        }
        System.out.println(sql);
        muonTras = getList(sql);
        return muonTras;
    }

    public static int interact(String sql) {
        int result = -1;
        try {
            ConnectDatabase connectDatabase = new ConnectDatabase();
            Connection connection = connectDatabase.getConnect();
            Statement statement = connection.createStatement();
            result = statement.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static ArrayList<TK> thongKe(String thuocTinh) throws SQLException {
        ConnectDatabase connectDatabase = new ConnectDatabase();
        ArrayList<TK> list = new ArrayList<>();
        String sql;
        ResultSet re;
        switch (thuocTinh) {
//            case "Ngày mượn":
//                sql = "select ngaymuon,count(*) from qltv.muontra group by ngaymuon";
//                re = connectDatabase.getConnect().
//                        createStatement().executeQuery(sql);
//                while (re.next()) {
//                    list.add(new TK(new SimpleDateFormat("dd-MM-yyyy").format(re.getDate("ngaymuon")),
//                            re.getInt("count(*)")));
//                }
//                connectDatabase.getConnect().close();
//                break;
            case "Ngày hẹn trả":
                sql = "select ngayhentra,count(*) from qltv.muontra group by ngayhentra";
                re = connectDatabase.getConnect().
                        createStatement().executeQuery(sql);
                while (re.next()) {
                    list.add(new TK(new SimpleDateFormat("dd-MM-yyyy").format(re.getDate("ngayhentra")),
                            re.getInt("count(*)")));
                }
                connectDatabase.getConnect().close();
                break;
            case "Tiền cọc":
                sql = "select tiencoc,count(*) from qltv.muontra group by tiencoc";
                re = connectDatabase.getConnect().
                        createStatement().executeQuery(sql);
                while (re.next()) {
                    list.add(new TK(re.getInt("tiencoc") + "",
                            re.getInt("count(*)")));
                }
                connectDatabase.getConnect().close();
                break;

        }

        return list;
    }

    public static ArrayList<TKR> thongKeRieng(String thuocTinh) throws SQLException {
        ConnectDatabase connectDatabase = new ConnectDatabase();
        ArrayList<TKR> list = new ArrayList<>();

        String sql;
        ResultSet re;
        switch (thuocTinh) {
            case "Mã độc giả":
                sql = "select madocgia,count(*) from qltv.muontra group by madocgia";
                re = connectDatabase.getConnect().
                        createStatement().executeQuery(sql);
                while (re.next()) {
                    TK tk = new TK(re.getString("madocgia"), re.getInt("count(*)"));
                    long tienCoc = DocGia.getTongTienCoc(re.getString("madocgia"));
                    long tienPhat = DocGia.getTongTienPhat(re.getString("madocgia"));
                    list.add(new TKR(tk, tienCoc, tienPhat));
                }
                connectDatabase.getConnect().close();
                break;
            case "Mã nhân viên":
                sql = "select manhanvien,count(*) from qltv.muontra group by manhanvien";
                re = connectDatabase.getConnect().
                        createStatement().executeQuery(sql);
                while (re.next()) {
                    TK tk = new TK(re.getString("manhanvien"), re.getInt("count(*)"));
                    long tienCoc = NhanVien.getTongTienCoc(re.getString("manhanvien"));
                    long tienPhat = NhanVien.getTongTienPhat(re.getString("manhanvien"));
                    list.add(new TKR(tk, tienCoc, tienPhat));
                }
                connectDatabase.getConnect().close();
                break;
            case "Ngày mượn":
                sql = "select ngaymuon,count(*), sum(tiencoc) from qltv.muontra group by ngaymuon";
                re = connectDatabase.getConnect().
                        createStatement().executeQuery(sql);
                while (re.next()) {
                    Date date = re.getDate("ngaymuon");
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(date);
                    TK tk = new TK(new SimpleDateFormat("dd-MM-yyyy").
                            format(date), re.getInt("count(*)"));
                    long tienCoc = re.getLong("sum(tiencoc)");
                    long tienPhat = getTongTienPhat(calendar.get(Calendar.DATE),
                            calendar.get(Calendar.MONTH) + 1, calendar.get(Calendar.YEAR));
                    list.add(new TKR(tk, tienCoc, tienPhat));
                }
                connectDatabase.getConnect().close();
                break;
            case "Tháng":
                // phương án là : lấy ra các tháng(cả năm) có trong csdl, 
                //sau đó trả về các ngày trong mỗi tháng cùng với thống kê của nó
                ArrayList<ThangNam> thangNams = new ArrayList<>();
                sql = "select distinct month(ngaymuon),year(ngaymuon) from muontra;";
                re = connectDatabase.getConnect().
                        createStatement().executeQuery(sql);
                while (re.next()) {
                    thangNams.add(new ThangNam(re.getInt("month(ngaymuon)"),
                            re.getInt("year(ngaymuon)")));
                }
                for (ThangNam thangNam : thangNams) {
                    //liệt kê tất cả thuộc tính cần thiết tháng xx, năm xxxx
                    sql = "select count(*), sum(tiencoc) "
                            + "from muontra where month(ngaymuon) = '" + thangNam.getThang() + "' "
                            + "and year(ngaymuon) = '" + thangNam.getNam() + "'";
                    re = connectDatabase.getConnect().
                            createStatement().executeQuery(sql);
                    while (re.next()) {
                        TK tk = new TK(thangNam.getThang() + "-"
                                + thangNam.getNam(), re.getInt("count(*)"));
                        long tienCoc = re.getLong("sum(tiencoc)");
                        long tienPhat = getTongTienPhat(0, thangNam.getThang(), thangNam.getNam());
                        list.add(new TKR(tk, tienCoc, tienPhat));
                    }
                }

                connectDatabase.getConnect().close();
                break;
            case "Năm":

                ArrayList<Integer> nams = new ArrayList<>();
                // phương án là : lấy ra các năm có trong csdl, 
                //sau đó trả về các ngày trong mỗi năm cùng với thống kê của nó
                sql = "select distinct year(ngaymuon) from muontra";
                re = connectDatabase.getConnect().
                        createStatement().executeQuery(sql);
                while (re.next()) {
                    nams.add(re.getInt("year(ngaymuon)"));
                }
                for (Integer nam : nams) {
                    //liệt kê tất cả thuộc tính cần thiết tháng xx, năm xxxx
                    sql = "select count(*), sum(tiencoc) from "
                            + "muontra where year(ngaymuon) = '" + nam + "'";
                    re = connectDatabase.getConnect().
                            createStatement().executeQuery(sql);
                    while (re.next()) {
                        TK tk = new TK(nam + "", re.getInt("count(*)"));
                        long tienCoc = re.getLong("sum(tiencoc)");
                        long tienPhat = getTongTienPhat(0, 0, nam);
                        list.add(new TKR(tk, tienCoc, tienPhat));
                    }
                }

                connectDatabase.getConnect().close();
                break;
        }
        return list;
    }

    public static MuonTra get(String ma) {
        MuonTra o = null;
        ConnectDatabase connectDatabase = new ConnectDatabase();
        try {
            ResultSet resultSet = connectDatabase.getConnect().
                    createStatement().executeQuery("select * from muontra where ma = '" + ma + "'");
            while (resultSet.next()) {
                o = new MuonTra(resultSet.getString("ma"),
                        resultSet.getString("madocgia"),
                        resultSet.getString("manhanvien"),
                        resultSet.getDate("ngaymuon"),
                        resultSet.getDate("ngayhentra"
                                + ""),
                        resultSet.getInt("tiencoc"));
            }
            connectDatabase.close();
        } catch (SQLException ex) {
            Logger.getLogger(MuonTra.class.getName()).log(Level.SEVERE, null, ex);
        }
        return o;
    }

    public static class ThangNam {

        private int thang;
        private int nam;

        public ThangNam() {
        }

        public ThangNam(int thang, int nam) {
            this.thang = thang;
            this.nam = nam;
        }

        public int getThang() {
            return thang;
        }

        public void setThang(int thang) {
            this.thang = thang;
        }

        public int getNam() {
            return nam;
        }

        public void setNam(int nam) {
            this.nam = nam;
        }

    }

    public static long getTongTienPhat(int ngay, int thang, int nam) {
        ConnectDatabase connectDatabase = new ConnectDatabase();
        String sql = "";
        if (ngay == 0) {
            if (thang == 0) {
                sql = "select sum(tienphat) from chitietmuontra"
                        + " where mamuontra in(\n"
                        + "select ma from muontra "
                        + "where year(ngaymuon) = '" + nam + "')";
            } else {
                sql = "select sum(tienphat) from chitietmuontra "
                        + "where mamuontra IN(\n"
                        + "select ma from muontra where "
                        + "month(ngaymuon) = '" + thang + "' "
                        + "and year(ngaymuon) = '" + nam + "')";
            }
        } else {
            System.out.println(ngay + "-" + thang + "-" + nam);
            sql = "select sum(tienphat) from muontra, chitietmuontra \n"
                    + "where chitietmuontra.mamuontra = muontra.ma\n"
                    + "and ngaymuon = '" + nam + "-" + thang + "-" + ngay + "'";
        }

        try {
            ResultSet re = connectDatabase.getConnect().
                    createStatement().executeQuery(sql);
            while (re.next()) {
                return re.getLong("sum(tienphat)");
            }
            connectDatabase.close();
        } catch (SQLException ex) {
            Logger.getLogger(NhanVien.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }

}
