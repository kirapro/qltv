/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package qltv.entity;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import qltv.ConnectDatabase;

/**
 *
 * @author Kira
 */
public class ChiTietMuonTra {

    private String maMuonTra;
    private String maSach;
    private int soLuong;
    private int tienPhat;
    private Date ngayTra;

    public ChiTietMuonTra() {
    }

    public ChiTietMuonTra(String maMuonTra, String maSach, int soLuong, int tienPhat, Date ngayTra) {
        this.maMuonTra = maMuonTra;
        this.maSach = maSach;
        this.soLuong = soLuong;
        this.tienPhat = tienPhat;
        this.ngayTra = ngayTra;
    }

    public String getMaMuonTra() {
        return maMuonTra;
    }

    public void setMaMuonTra(String maMuonTra) {
        this.maMuonTra = maMuonTra;
    }

    public String getMaSach() {
        return maSach;
    }

    public void setMaSach(String maSach) {
        this.maSach = maSach;
    }

    public int getSoLuong() {
        return soLuong;
    }

    public void setSoLuong(int soLuong) {
        this.soLuong = soLuong;
    }

    public int getTienPhat() {
        return tienPhat;
    }

    public void setTienPhat(int tienPhat) {
        this.tienPhat = tienPhat;
    }

    public Date getNgayTra() {
        return ngayTra;
    }

    public void setNgayTra(Date ngayTra) {
        this.ngayTra = ngayTra;
    }

    public static ArrayList<ChiTietMuonTra> getList(String sql) {
        ArrayList<ChiTietMuonTra> list = new ArrayList<>();
        ConnectDatabase connectDatabase = new ConnectDatabase();
        Connection connection = connectDatabase.getConnect();
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                String mamuontra = resultSet.getString("mamuontra");
                String masach = resultSet.getString("masach");
                int soluong = resultSet.getInt("soluong");
                int tienphat = resultSet.getInt("tienphat");
                Date ngaytra = resultSet.getDate("ngaytra");
                list.add(new ChiTietMuonTra(
                        mamuontra,
                        masach,
                        soluong,
                        tienphat,
                        ngaytra));
            }
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    public static int add(ChiTietMuonTra o) {
        Sach sach = Sach.get(o.getMaSach());
        if (sach.getSoLuong() >= o.getSoLuong()) {
            String sql = "insert into chitietmuontra values('"
                    + o.getMaMuonTra() + "', '"
                    + o.getMaSach() + "', "
                    + o.getSoLuong() + ", null, null )";
            return interact(sql);
        } else {
            return -2;
        }

    }

    public static int update(ChiTietMuonTra o) {
        String sql = "";
        if (o.getNgayTra() == null) {
            sql = "update chitietmuontra set soluong = " + o.getSoLuong()
                    + ", ngaytra = NULL, tienphat = NULL"
                    + " where mamuontra = '" + o.getMaMuonTra()
                    + "' and masach = '" + o.getMaSach() + "'";
        } else {
            sql = "update chitietmuontra set soluong = "
                    + o.getSoLuong() + ", tienphat = "
                    + o.getTienPhat() + ", ngaytra = '"
                    + new SimpleDateFormat("yyyy-MM-dd").format(o.getNgayTra())
                    + "' where mamuontra = '" + o.getMaMuonTra()
                    + "' and masach = '" + o.getMaSach() + "'";
        }
        System.out.println(sql);
        return interact(sql);
    }

    public static int delete(ChiTietMuonTra o) {
        String sql = "delete from chitietmuontra where mamuontra = '"
                + o.getMaMuonTra() + "' and masach = '" + o.getMaSach() + "'";
        return interact(sql);
    }

    public static ArrayList<ChiTietMuonTra> search(ChiTietMuonTra o) {
        String sql = "select * from chitietmuontra where mamuontra = '" + o.getMaMuonTra() + "'";
        if (!o.getMaSach().equals("")) {
            sql += " and masach = '" + o.getMaSach() + "'";
        }
        if (o.getSoLuong() != -1) {
            sql += " and soluong = " + o.getSoLuong();
        }
        if (o.getTienPhat() != 0) {
            sql += " and tienphat = " + o.getTienPhat();
        }
        if (o.getNgayTra() != null) {
            sql += " and ngaytra = '" + new SimpleDateFormat("yyyy-MM-dd").format(o.getNgayTra()) + "'";
        }
        if (o.getMaSach().equals("")
                && o.getSoLuong() == -1
                && o.getTienPhat() == -1
                && new SimpleDateFormat("yyyy-MM-dd")
                        .format(o.getNgayTra()).equals("1900-01-01")) {
            sql = "select * from chitietmuontra where mamuontra = MT000";
        }
        return getList(sql);
    }

    public static int interact(String sql) {
        int result = -1;
        try {
            ConnectDatabase connectDatabase = new ConnectDatabase();
            Connection connection = connectDatabase.getConnect();
            Statement statement = connection.createStatement();
            result = statement.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static ArrayList<TK> thongKe(String thuocTinh, String maMT) throws SQLException {
        ConnectDatabase connectDatabase = new ConnectDatabase();
        ArrayList<TK> list = new ArrayList<>();
        String sql;
        ResultSet re;
        switch (thuocTinh) {
            case "Mã sách":
                sql = "select masach,count(*) from qltv.chitietmuontra "
                        + "where mamuontra = '" + maMT + "' group by masach";
                re = connectDatabase.getConnect().
                        createStatement().executeQuery(sql);
                while (re.next()) {
                    list.add(new TK(re.getString("masach") + "",
                            re.getInt("count(*)")));
                }
                connectDatabase.getConnect().close();
                break;
            case "Số lượng":
                sql = "select soluong,count(*) from qltv.chitietmuontra "
                        + "where mamuontra = '" + maMT + "' group by soluong";
                re = connectDatabase.getConnect().
                        createStatement().executeQuery(sql);
                while (re.next()) {
                    list.add(new TK(re.getInt("soluong") + "",
                            re.getInt("count(*)")));
                }
                connectDatabase.getConnect().close();
                break;
//            case "Tiền phạt":
//                sql = "select tienphat,count(*) from qltv.chitietmuontra "
//                        + "where mamuontra = '" + maMT + "' group by tienphat";
//                re = connectDatabase.getConnect().
//                        createStatement().executeQuery(sql);
//                while (re.next()) {
//                    list.add(new TK(re.getInt("tienphat") + "",
//                            re.getInt("count(*)")));
//                }
//                connectDatabase.getConnect().close();
//                break;
//            case "Ngày trả":
//                sql = "select ngaytra,count(*) from qltv.chitietmuontra "
//                        + "where mamuontra = '" + maMT + "' group by ngaytra";
//                re = connectDatabase.getConnect().
//                        createStatement().executeQuery(sql);
//                while (re.next()) {
//                    list.add(new TK(new SimpleDateFormat("dd-MM-yyyy")
//                            .format(re.getDate("ngaytra")),
//                            re.getInt("count(*)")));
//                }
//                connectDatabase.getConnect().close();
//                break;
        }

        return list;
    }

    public static ChiTietMuonTra get(String maMT, String maS) {
        ChiTietMuonTra o = null;
        ConnectDatabase connectDatabase = new ConnectDatabase();
        try {
            ResultSet resultSet = connectDatabase.getConnect().
                    createStatement().executeQuery("select * from chitietmuontra where mamuontra = '"
                            + maMT + "' and masach = '" + maS + "'");
            while (resultSet.next()) {
                String mamuontra = resultSet.getString("mamuontra");
                String masach = resultSet.getString("masach");
                int soluong = resultSet.getInt("soluong");
                int tienphat = resultSet.getInt("tienphat");
                Date ngaytra = resultSet.getDate("ngaytra");
                o = new ChiTietMuonTra(
                        mamuontra,
                        masach,
                        soluong,
                        tienphat,
                        ngaytra);
            }
            connectDatabase.close();
        } catch (SQLException ex) {
            Logger.getLogger(ChiTietMuonTra.class.getName()).log(Level.SEVERE, null, ex);
        }
        return o;
    }

    public void setTienPhat(Object object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
