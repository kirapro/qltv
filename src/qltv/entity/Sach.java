/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package qltv.entity;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import qltv.ConnectDatabase;

/**
 *
 * @author Kira
 */
public class Sach {

    private String ma;
    private String ten;
    private String loaiSach;
    private String tacGia;
    private String nhaXuatBan;
    private int namXuatBan;
    private String viTri;
    private int soLuong;

    public Sach() {
    }

    public Sach(String ma, String ten, String loaiSach, String tacGia, String nhaXuatBan, int namXuatBan, String viTri, int soLuong) {
        this.ma = ma;
        this.ten = ten;
        this.loaiSach = loaiSach;
        this.tacGia = tacGia;
        this.nhaXuatBan = nhaXuatBan;
        this.namXuatBan = namXuatBan;
        this.viTri = viTri;
        this.soLuong = soLuong;
    }

    public String getMa() {
        return ma;
    }

    public void setMa(String ma) {
        this.ma = ma;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public String getLoaiSach() {
        return loaiSach;
    }

    public void setLoaiSach(String loaiSach) {
        this.loaiSach = loaiSach;
    }

    public String getTacGia() {
        return tacGia;
    }

    public void setTacGia(String tacGia) {
        this.tacGia = tacGia;
    }

    public String getNhaXuatBan() {
        return nhaXuatBan;
    }

    public void setNhaXuatBan(String nhaXuatBan) {
        this.nhaXuatBan = nhaXuatBan;
    }

    public int getNamXuatBan() {
        return namXuatBan;
    }

    public void setNamXuatBan(int namXuatBan) {
        this.namXuatBan = namXuatBan;
    }

    public String getViTri() {
        return viTri;
    }

    public void setViTri(String viTri) {
        this.viTri = viTri;
    }

    public int getSoLuong() {
        return soLuong;
    }

    public void setSoLuong(int soLuong) {
        this.soLuong = soLuong;
    }

    

    public static ArrayList<Sach> getList(String sql) {
        ArrayList<Sach> list = new ArrayList<>();
        ConnectDatabase connectDatabase = new ConnectDatabase();
        Connection connection = connectDatabase.getConnect();
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                list.add(new Sach(resultSet.getString("ma"),
                        resultSet.getString("ten"),
                        resultSet.getString("loaisach"),
                        resultSet.getString("tacgia"),
                        resultSet.getString("nhaxuatban"),
                        resultSet.getInt("namxuatban"),
                        resultSet.getString("vitri"),
                        resultSet.getInt("soluong")));
            }
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    public static int add(Sach o) {
        String sql = "insert into sach values('"
                + o.getMa() + "', '"
                + o.getTen() + "', '"
                + o.getLoaiSach() + "', '"
                + o.getTacGia() + "', '"
                + o.getNhaXuatBan() + "', "
                + o.getNamXuatBan() + ", '"
                + o.getViTri() + "', "
                + o.getSoLuong()
                + ")";
        return interact(sql);
    }

    public static int update(Sach o) {
        String sql = "update sach set ten = '"
                + o.getTen() + "', loaisach = '"
                + o.getLoaiSach() + "', tacgia = '"
                + o.getTacGia() + "', nhaxuatban = '"
                + o.getNhaXuatBan() + "', namxuatban = "
                + o.getNamXuatBan() + ", vitri = '"
                + o.getViTri() + "', soluong = "
                + o.getSoLuong()
                + " where ma = '" + o.getMa() + "'";
        return interact(sql);
    }

    public static int delete(Sach o) {
        String sql = "delete from sach where ma = '"
                + o.getMa() + "' and ten = '"
                + o.getTen() + "' and loaisach = '"
                + o.getLoaiSach() + "' and tacgia = '"
                + o.getTacGia() + "' and nhaxuatban = '"
                + o.getNhaXuatBan() + "' and namxuatban = "
                + o.getNamXuatBan() + " and vitri = '"
                + o.getViTri() + "' and soluong = "
                + o.getSoLuong();
        return interact(sql);
    }

    public static ArrayList<Sach> search(Sach o) {
        String sql = "select * from sach where";
        if (!o.getMa().equals("")) {
            sql += " ma = '" + o.getMa() + "'";
            if (!o.getTen().equals("")) {
                sql += " and ten = '" + o.getTen() + "'";
            }
            if (!o.getLoaiSach().equals("")) {
                sql += " and loaisach = '" + o.getLoaiSach() + "'";
            }
            if (!o.getTacGia().equals("")) {
                sql += " and tacgia = '" + o.getTacGia() + "'";
            }
            if (!o.getNhaXuatBan().equals("")) {
                sql += " and nhaxuatban = '" + o.getNhaXuatBan() + "'";
            }
            if (o.getNamXuatBan() != -1) {
                sql += " and namxuatban = " + o.getNamXuatBan();
            }
            if (!o.getViTri().equals("")) {
                sql += " and vitri = '" + o.getViTri() + "'";
            }
            if (o.getSoLuong() != -1) {
                sql += " and soluong = " + o.getSoLuong();
            }
        } else {
            if (!o.getTen().equals("")) {
                sql += " ten = '" + o.getTen() + "'";
                if (!o.getLoaiSach().equals("")) {
                    sql += " and loaisach = '" + o.getLoaiSach() + "'";
                }
                if (!o.getTacGia().equals("")) {
                    sql += " and tacgia = '" + o.getTacGia() + "'";
                }
                if (!o.getNhaXuatBan().equals("")) {
                    sql += " and nhaxuatban = '" + o.getNhaXuatBan() + "'";
                }
                if (o.getNamXuatBan() != -1) {
                    sql += " and namxuatban = " + o.getNamXuatBan();
                }
                if (!o.getViTri().equals("")) {
                    sql += " and vitri = '" + o.getViTri() + "'";
                }
                if (o.getSoLuong() != -1) {
                    sql += " and soluong = " + o.getSoLuong();
                }
            } else {
                if (!o.getLoaiSach().equals("")) {
                    sql += " loaisach = '" + o.getLoaiSach() + "'";
                    if (!o.getTacGia().equals("")) {
                        sql += " and tacgia = '" + o.getTacGia() + "'";
                    }
                    if (!o.getNhaXuatBan().equals("")) {
                        sql += " and nhaxuatban = '" + o.getNhaXuatBan() + "'";
                    }
                    if (o.getNamXuatBan() != -1) {
                        sql += " and namxuatban = " + o.getNamXuatBan();
                    }
                    if (!o.getViTri().equals("")) {
                        sql += " and vitri = '" + o.getViTri() + "'";
                    }
                    if (o.getSoLuong() != -1) {
                        sql += " and soluong = " + o.getSoLuong();
                    }
                } else {
                    if (!o.getTacGia().equals("")) {
                        sql += " tacgia = '" + o.getTacGia() + "'";
                        if (!o.getNhaXuatBan().equals("")) {
                            sql += " and nhaxuatban = '" + o.getNhaXuatBan() + "'";
                        }
                        if (o.getNamXuatBan() != -1) {
                            sql += " and namxuatban = " + o.getNamXuatBan();
                        }
                        if (!o.getViTri().equals("")) {
                            sql += " and vitri = '" + o.getViTri() + "'";
                        }
                        if (o.getSoLuong() != -1) {
                            sql += " and soluong = " + o.getSoLuong();
                        }
                    } else {
                        if (!o.getNhaXuatBan().equals("")) {
                            sql += " nhaxuatban = '" + o.getNhaXuatBan() + "'";
                            if (o.getNamXuatBan() != -1) {
                                sql += " and namxuatban = " + o.getNamXuatBan();
                            }
                            if (!o.getViTri().equals("")) {
                                sql += " and vitri = '" + o.getViTri() + "'";
                            }
                            if (o.getSoLuong() != -1) {
                                sql += " and soluong = " + o.getSoLuong();
                            }
                        } else {
                            if (o.getNamXuatBan() != -1) {
                                sql += " namxuatban = " + o.getNamXuatBan();
                                if (!o.getViTri().equals("")) {
                                    sql += " and vitri = '" + o.getViTri() + "'";
                                }
                                if (o.getSoLuong() != -1) {
                                    sql += " and soluong = " + o.getSoLuong();
                                }
                            } else {
                                if (!o.getViTri().equals("")) {
                                    sql += " vitri = '" + o.getViTri() + "'";
                                    if (o.getSoLuong() != -1) {
                                        sql += " and soluong = " + o.getSoLuong();
                                    }
                                } else {
                                    if (o.getSoLuong() != -1) {
                                        sql += " soluong = " + o.getSoLuong();
                                    } else {
                                        return null;
                                    }
                                }

                            }
                        }
                        return getList(sql);
                    }

                }
            }
        }
        System.out.println(sql);
        return getList(sql);
    }

    public static int interact(String sql) {
        int result = -1;
        try {
            ConnectDatabase connectDatabase = new ConnectDatabase();
            Connection connection = connectDatabase.getConnect();
            Statement statement = connection.createStatement();
            result = statement.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static ArrayList<TK> thongKe(String thuocTinh) throws SQLException {
        ConnectDatabase connectDatabase = new ConnectDatabase();
        ArrayList<TK> list = new ArrayList<>();
        String sql;
        ResultSet re;
        switch (thuocTinh) {
            case "Tên":
                sql = "select ten,count(*) from qltv.sach group by ten";
                re = connectDatabase.getConnect().
                        createStatement().executeQuery(sql);
                while (re.next()) {
                    list.add(new TK(re.getString("ten") + "",
                            re.getInt("count(*)")));
                }
                connectDatabase.getConnect().close();
                break;
            case "Loại sách":
                sql = "select loaisach,count(*) from qltv.sach group by loaisach";
                re = connectDatabase.getConnect().
                        createStatement().executeQuery(sql);
                while (re.next()) {
                    list.add(new TK(re.getString("loaisach") + "",
                            re.getInt("count(*)")));
                }
                connectDatabase.getConnect().close();
                break;
            case "Tác giả":
                sql = "select tacgia,count(*) from qltv.sach group by tacgia";
                re = connectDatabase.getConnect().
                        createStatement().executeQuery(sql);
                while (re.next()) {
                    list.add(new TK(re.getString("tacgia") + "",
                            re.getInt("count(*)")));
                }
                connectDatabase.getConnect().close();
                break;
            case "Nhà xuất bản":
                sql = "select nhaxuatban,count(*) from qltv.sach group by nhaxuatban";
                re = connectDatabase.getConnect().
                        createStatement().executeQuery(sql);
                while (re.next()) {
                    list.add(new TK(re.getString("nhaxuatban") + "",
                            re.getInt("count(*)")));
                }
                connectDatabase.getConnect().close();
                break;
            case "Năm xuất bản":
                sql = "select namxuatban,count(*) from qltv.sach group by namxuatban";
                re = connectDatabase.getConnect().
                        createStatement().executeQuery(sql);
                while (re.next()) {
                    list.add(new TK(re.getString("namxuatban") + "",
                            re.getInt("count(*)")));
                }
                connectDatabase.getConnect().close();
                break;
            case "Vị trí":
                sql = "select vitri,count(*) from qltv.sach group by vitri";
                re = connectDatabase.getConnect().
                        createStatement().executeQuery(sql);
                while (re.next()) {
                    list.add(new TK(re.getString("vitri") + "",
                            re.getInt("count(*)")));
                }
                connectDatabase.getConnect().close();
                break;
            case "Số lượng":
                sql = "select soluong,count(*) from qltv.sach group by soluong";
                re = connectDatabase.getConnect().
                        createStatement().executeQuery(sql);
                while (re.next()) {
                    list.add(new TK(re.getString("soluong") + "",
                            re.getInt("count(*)")));
                }
                connectDatabase.getConnect().close();
                break;
        }

        return list;
    }

    public static Sach get(String ma) {
        Sach o = null;
        ConnectDatabase connectDatabase = new ConnectDatabase();
        try {
            ResultSet resultSet = connectDatabase.getConnect().
                    createStatement().executeQuery("select * from sach where ma = '" + ma + "'");
            while (resultSet.next()) {
                o = new Sach(resultSet.getString("ma"),
                        resultSet.getString("ten"),
                        resultSet.getString("loaisach"),
                        resultSet.getString("tacgia"),
                        resultSet.getString("nhaxuatban"),
                        resultSet.getInt("namxuatban"),
                        resultSet.getString("vitri"),
                        resultSet.getInt("soluong"));
            }
            connectDatabase.close();
        } catch (SQLException ex) {
            Logger.getLogger(DocGia.class.getName()).log(Level.SEVERE, null, ex);
        }
        return o;
    }
}
