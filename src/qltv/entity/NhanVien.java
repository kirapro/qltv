/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package qltv.entity;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import qltv.ConnectDatabase;

/**
 *
 * @author Kira
 */
public class NhanVien {

    private String ma;
    private String ten;
    private long sdt;
    private String diaChi;

    public NhanVien(String ma, String ten, long sdt, String diaChi) {
        this.ma = ma;
        this.ten = ten;
        this.sdt = sdt;
        this.diaChi = diaChi;
    }

    public NhanVien() {
    }

    public String getMa() {
        return ma;
    }

    public void setMa(String ma) {
        this.ma = ma;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public long getSdt() {
        return sdt;
    }

    public void setSdt(long sdt) {
        this.sdt = sdt;
    }

    public String getDiaChi() {
        return diaChi;
    }

    public void setDiaChi(String diaChi) {
        this.diaChi = diaChi;
    }

    public static ArrayList<NhanVien> getList(String sql) {
        ArrayList<NhanVien> list = new ArrayList<>();
        ConnectDatabase connectDatabase = new ConnectDatabase();
        Connection connection = connectDatabase.getConnect();
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                list.add(new NhanVien(
                        resultSet.getString("ma"),
                        resultSet.getString("ten"),
                        resultSet.getLong("sdt"),
                        resultSet.getString("diachi")));
            }
            connection.close();
        } catch (SQLException e) {
        }
        return list;
    }

    public static int add(NhanVien o) {
        String sql = "insert into nhanvien values('"
                + o.getMa() + "', '"
                + o.getTen() + "', "
                + o.getSdt() + ", '"
                + o.getDiaChi() + "'"
                + ")";
        return interact(sql);
    }

    public static int update(NhanVien o) {
        String sql = "update nhanvien set ten = '"
                + o.getTen() + "', sdt = "
                + o.getSdt() + ", diachi = '"
                + o.getDiaChi() + "'"
                + " where ma = '" + o.getMa() + "'";
        return interact(sql);
    }

    public static int delete(NhanVien o) {
        String sql = "delete from nhanvien where ma = '"
                + o.getMa() + "' and ten = '"
                + o.getTen() + "' and sdt = "
                + o.getSdt() + " and diachi = '"
                + o.getDiaChi() + "'";
        return interact(sql);
    }

    public static ArrayList<NhanVien> search(NhanVien o) {
        String sql = "select * from nhanvien where";
        if (!o.getMa().equals("")) {
            sql += " ma = '" + o.getMa() + "'";
            if (!o.getTen().equals("")) {
                sql += " and ten = '" + o.getTen() + "'";
            }
            if (o.getSdt() != -1) {
                sql += " and sdt = " + o.getSdt();
            }
            if (!o.getDiaChi().equals("")) {
                sql += " and diachi = '" + o.getDiaChi() + "'";
            }
        } else {
            if (!o.getTen().equals("")) {
                sql += " ten = '" + o.getTen() + "'";
                if (o.getSdt() != -1) {
                    sql += " and sdt = " + o.getSdt();
                }
                if (!o.getDiaChi().equals("")) {
                    sql += " and diachi = '" + o.getDiaChi() + "'";
                }
            } else {
                if (o.getSdt() != -1) {
                    sql += " sdt = " + o.getSdt();
                    if (!o.getDiaChi().equals("")) {
                        sql += " and diachi = '" + o.getDiaChi() + "'";
                    }
                } else {
                    if (!o.getDiaChi().equals("")) {
                        sql += " diachi = '" + o.getDiaChi() + "'";
                    } else {
                        return null;
                    }
                }

            }
        }
        return getList(sql);

    }

    public static int interact(String sql) {
        int result = -1;
        try {
            ConnectDatabase connectDatabase = new ConnectDatabase();
            Connection connection = connectDatabase.getConnect();
            Statement statement = connection.createStatement();
            result = statement.executeUpdate(sql);
        } catch (SQLException e) {
        }
        return result;
    }

    public static ArrayList<TK> thongKe(String thuocTinh) throws SQLException {
        ConnectDatabase connectDatabase = new ConnectDatabase();
        ArrayList<TK> list = new ArrayList<>();
        String sql;
        ResultSet re;
        switch (thuocTinh) {
            case "Tên":
                sql = "select ten,count(*) from nhanvien group by ten";
                re = connectDatabase.getConnect().
                        createStatement().executeQuery(sql);
                while (re.next()) {
                    list.add(new TK(re.getString("ten") + "",
                            re.getInt("count(*)")));
                }
                connectDatabase.getConnect().close();
                break;
            case "Số điện thoại":
                sql = "select sdt,count(*) from nhanvien group by sdt";
                re = connectDatabase.getConnect().
                        createStatement().executeQuery(sql);
                while (re.next()) {
                    list.add(new TK(re.getLong("sdt") + "",
                            re.getInt("count(*)")));
                }
                connectDatabase.getConnect().close();
                break;
            case "Địa chỉ":
                sql = "select diachi,count(*) from nhanvien group by diachi";
                re = connectDatabase.getConnect().
                        createStatement().executeQuery(sql);
                while (re.next()) {
                    list.add(new TK(re.getString("diachi") + "",
                            re.getInt("count(*)")));
                }
                connectDatabase.getConnect().close();
                break;
        }

        return list;
    }

    public static NhanVien get(String ma) {
        NhanVien o = null;
        ConnectDatabase connectDatabase = new ConnectDatabase();
        try {
            ResultSet re = connectDatabase.getConnect().
                    createStatement().executeQuery("select * from nhanvien where ma = '" + ma + "'");
            while (re.next()) {
                o = new NhanVien(re.getString("ma"),
                        re.getString("ten"),
                        re.getLong("sdt"),
                        re.getString("diachi"));
            }
            connectDatabase.close();
        } catch (SQLException ex) {
            Logger.getLogger(NhanVien.class.getName()).log(Level.SEVERE, null, ex);
        }
        return o;
    }

    public static long getTongTienCoc(String ma) {
        ConnectDatabase connectDatabase = new ConnectDatabase();
        try {
            ResultSet re = connectDatabase.getConnect().
                    createStatement().executeQuery("select sum(tiencoc)"
                            + " from muontra where manhanvien = '" + ma + "'");
            while (re.next()) {
                return re.getLong("sum(tiencoc)");
            }
            connectDatabase.close();
        } catch (SQLException ex) {
            Logger.getLogger(NhanVien.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }

    public static long getTongTienPhat(String ma) {
        ConnectDatabase connectDatabase = new ConnectDatabase();
        try {
            ResultSet re = connectDatabase.getConnect().
                    createStatement().executeQuery("select sum(tienphat) from muontra, "
                            + "chitietmuontra where muontra.ma ="
                            + " chitietmuontra.mamuontra and manhanvien = '" + ma + "'");
            while (re.next()) {
                return re.getLong("sum(tienphat)");
            }
            connectDatabase.close();
        } catch (SQLException ex) {
            Logger.getLogger(NhanVien.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }
}
